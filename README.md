This project was created for D3's challenge. 

The scope of the project was develop a logic to list the Covid 19's cases, according the number of days inputted by user.

I had some limitations with Docker and Unit Tests. But I tried to learn something more about it.

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### yarn docker:build or npm run docker:build
Before start, it's necessary to create the image on docker. So, for that just run the script `docker:build`.

See more about [build command](https://docs.docker.com/engine/reference/commandline/build/)


### yarn docker:run or npm run docker:run

**(before that, execute the cmd `docker:build`)**

After build, to `run` the image, just execute the command `docker:run`.

The application will run on [localhost](http://localhost:3001)

See more about [run command](https://docs.docker.com/engine/reference/commandline/run/)

## See more on documentation
[Documentation](./docs/index.html)

## Other scripts

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

