import React from 'react';

/**
 * Feedback message to user knows whats is happening.
 * @param {boolean} fetching Status if request is running
 * @param {boolean} error Status if request returns some kind of error
 * @returns If some of values is true, returns the message with respective style. Else, returns null.
 */
function Alert ({ fetching, error }) {
    if (fetching) {
        return (
            <div 
                className="alert alert-info" 
                role="alert">
                <span 
                    className="glyphicon glyphicon-exclamation-sign" 
                    aria-hidden="true"/>
                Aguarde. Estamos buscando os dados.
            </div>
        )
    }
    if (error) {
        return (
            <div 
                className="alert alert-danger" 
                role="alert">
                <span 
                    className="glyphicon glyphicon-exclamation-sign" 
                    aria-hidden="true"/>
                Ops! Parece que aconteceu um erro.
            </div>
        )
    }
    return null
}

export default Alert