import React from 'react';

/**
 * Input of count of days to show
 * @param {function} onChange Change event when value changes
 * @param {function} onSubmit Submit event sent when user click in button or press enter. Only enabled if value is valid and bigger then 0.
 * @param {number} value count of days choose by user
 */
const Input = ({ onChange, onSubmit, value }) => {
    const enabled = value > 0;

    const onChangeValue = (ev) => {
        ev.preventDefault();
        try {
            const { value: newValue } = ev.target;
            if (!/^\d+$/.test(newValue)) {
                throw new Error();
            }
            onChange(+newValue);
        } catch(e) {}
    }

    const onSubmitForm = (ev) => {
        ev.preventDefault();
        if (enabled) {
            onSubmit();
        }
    }

    return (
        <form 
            className="form" 
            onSubmit={onSubmitForm}>
            <div 
                className="form-group">
                <label 
                    htmlFor="number-of-days">Dias para exibir:</label>
                <input 
                    type="number" 
                    step={1}
                    min={1}
                    className="form-control" 
                    id="number-of-days" 
                    placeholder="nº de dias"
                    value={value}
                    required
                    defaultValue={1}
                    onChange={onChangeValue} />
            </div>
            <button 
                type="submit" 
                className={`btn btn-${enabled ? 'primary' : 'secondary'}`}
                disabled={!enabled}>Pesquisar</button>
        </form>  
    );
}

export default Input;
