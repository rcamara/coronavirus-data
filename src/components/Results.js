import React from 'react';
import format from 'date-fns/format'
import { pt } from 'date-fns/locale'

import { numberWithCommas } from '../helpers/numberWithCommas'

/**
 * Render table with Covid 19's cases.
 * @param {array|enum} data List of Covid 19's cases.
 * @return Table with cases. If data is null or hasn't length returns null;   
 */
function Results({ data }) {
    
    if (!data?.length) return null;

    const renderRow = (row, index) => {
        const date = new Date(row.Date);
        const id = format(date, 'dd-MM-yyyy')
        return (
            <tr key={id}>
                <td><strong>{index + 1}.</strong></td>
                <td>
                    {format(date, "dd MMMM yyyy", {
                        locale: pt
                    })}
                </td>
                <td>{numberWithCommas(row.Confirmed)}</td>
                <td>{numberWithCommas(row.Active)}</td>
                <td>{numberWithCommas(row.Recovered)}</td>
                <td>{numberWithCommas(row.Deaths)}</td>
            </tr>
        )
    }
  return (
    <table className="table table- table-bordered">
        <thead className="thead-dark">
            <tr>
                <th>#</th>
                <th>Data</th>
                <th>Casos Confirmados</th>
                <th>Casos Ativos</th>
                <th>Casos de mortes</th>
                <th>Casos recuperados</th>
            </tr>
        </thead>
        <tbody>
            {data?.map(renderRow)}
        </tbody>
    </table>
  );
}

export default Results;
