import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Alert from "./Alert";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with some status", () => {
  act(() => {
    render(<Alert fetching={true} />, container);
  });
  expect(container.textContent).toBe("Aguarde. Estamos buscando os dados.");

  act(() => {
    render(<Alert error={true} />, container);
  });
  expect(container.textContent).toBe("Ops! Parece que aconteceu um erro.");

  act(() => {
    render(<Alert fetching={true} error={true} />, container);
  });
  expect(container.textContent).toBe("Aguarde. Estamos buscando os dados.");
});