import addDays from 'date-fns/addDays'
import fromUnixTime from 'date-fns/fromUnixTime'
import formatISO from 'date-fns/formatISO'
import config from '../config'

export const api = {
    /**
     * Request to retrieve the list of days with corresponding cases of Covid.
     * Getting a period based on number of days and starts at day
     * 
     * @param {Object} payload Payload to send to api
     * @param {number} payload.days Count of days to retrieve
     * @param {string|date} payload.startsAt Date where starts count of days
     * @returns Request response 
     */
    
    list: async ({ days = 1, startsAt = config.fistDate }) => {
        const params = new URLSearchParams();
        const from = fromUnixTime(startsAt);
        const to = addDays(from, +days)

        params.set('from', formatISO(from));
        params.set('to', formatISO(to))

        const url = `${config.api}/country/brazil`
        const endpoint = `${url}?${params.toString()}`;
        const response = await fetch(endpoint)
        return response.json()
        
    }
}