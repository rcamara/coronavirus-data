/**
 * @param {number} number Value to convert and add commas
 * @returns Value converted
 */
export const numberWithCommas = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}