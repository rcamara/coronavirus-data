import React from 'react';

const STATUS = {
    IDDLE: 'iddle',
    FETCHING: 'fetching',
    ERROR: 'error',
    SUCCESS: 'success',
}

export const useQuery = (request, params) => {
    const [status, setStatus] = React.useState(STATUS.IDDLE)
    const [response, setReponse] = React.useState()

    const fetch = React.useCallback(async () => {
        try {
            setStatus(STATUS.FETCHING);
            const response = await request(params);
            setReponse(response);
            setStatus(STATUS.SUCCESS);
        } catch(e) {
            setStatus(STATUS.ERROR)
        }
    }, [request, params])

    return {
        error: STATUS.ERROR === status,
        success: STATUS.SUCCESS === status,
        fetching: STATUS.FETCHING === status,
        iddle: STATUS.IDDLE === status,
        epoch: Date.now(),
        data: response,
        fetch: fetch,
    }
}