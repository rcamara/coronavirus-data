import 'url-search-params-polyfill';
import 'bootstrap/dist/css/bootstrap.min.css'

import React from 'react';

import Container from './components/Container';
import Input from './components/Input'
import Results from './components/Results'
import Alert from './components/Alert'

import { api } from './services/api'

import { useQuery } from './helpers/useQuery'

function App() {
  const [days, setDays] = React.useState(1);
  const { data, fetch, error, fetching } = useQuery(api.list, {
    days,
  })
  
  const onRetrieveList = () => fetch();
  
  return (
    <>
      <div className="jumbotron jumbotron-fluid">
        <Container>
          <h1 className="display-4">Covid-19 no Brasil</h1>
          <p className="lead">Digite abaixo, a quantidade de dias que deseja que sejam exibidos.</p>
        </Container>
      </div>
      <Container>
        <div className="row">
          <div className="col-md-3">
            <Input value={days} onChange={setDays} onSubmit={onRetrieveList} />  
          </div>
          <div className="col-md-9">
            <Alert fetching={fetching} error={error} />
            <Results data={data} />
          </div>
        </div>
      </Container>
    </>
  );
}

export default App;
