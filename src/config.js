/**
 * @returns {Object} Config with first date and api url
 */

const config = {
    fistDate: 1579651200,
    api: process.env.REACT_APP_API
}

export default config;